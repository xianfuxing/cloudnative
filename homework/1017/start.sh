#!/bin/bash

docker build . -t httpserver:1.0
docker run -d --name goweb -p 8081:8081 httpserver:1.0

# uncomment push docker hub
# docker tag httpserver:1.0 dohello/httpserver:1.0
# docker login
# docker push dohello/httpserver:1.0

# ip addr
PID=$(docker inspect --format "{{ .State.Pid }}" goweb)
nsenter --target $PID -n ip addr
