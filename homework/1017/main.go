package main

import (
	"fmt"
	"log"
	"net/http"
	"os"
	"strings"
)

func init() {
	os.Setenv("version", "1.0.0")
}

func RootHandler(w http.ResponseWriter, r *http.Request) {
	fmt.Println("=========== Request Header ===========")
	fmt.Println(r.Header)

	// read request header, write to response
	for k, v := range r.Header {
		//fmt.Printf("%s: %s\n", k, v)
		w.Header().Set(k, strings.Join(v, ";"))

		body := fmt.Sprintf("%s: %s\n", k, v)
		w.Write([]byte(body))
	}
	// write version
	w.Header().Set("Version", os.Getenv("version"))

	fmt.Println()
	fmt.Println("=========== Response Header ===========")
	fmt.Println(w.Header())
	log.Printf("[%s] %s \t IP: %s", r.Method, r.URL, r.RemoteAddr)
}

func HealthzHandler(w http.ResponseWriter, r *http.Request) {
	w.Write([]byte("200"))
	log.Printf("[%s] %s \t IP: %s", r.Method, r.URL, r.RemoteAddr)
}

func main() {
	http.HandleFunc("/", RootHandler)
	http.HandleFunc("/healthz", HealthzHandler)

	http.ListenAndServe("0.0.0.0:8081", nil)

}
